# Search-for-fruits


## Pre-installation Software Requirements
> 1. [git v2.10.x+](https://git-scm.com/)
>
> 2. [docker](https://docs.docker.com/engine/installation/)
>
> 3. [yarn](https://yarnpkg.com/lang/en/docs/install/)
>
> 4. [docker-compose](https://docs.docker.com/compose/install/)
>



## Project Installation
> **Project Installation occurs from terminal or command prompt**
>
> 1. Clone project from gitlab
>
> 2. Navigate to new project directory
>    `cd <path-to-cloned-project-directory>`
>
> 3. Navigate to frontend folder (search-for-fruits)
>    `cd search-for-fruits`
>
> 4. Package installation (cloning git will also include the node_modules in the project structure. No need to install explicitly)
>    `yarn install`
>
> 5. Build dist watcher (cloning git will also include the dist in the project structure. No need to build explicitly)
>    `yarn build:local`
>
> 6. Navigate back to new project directory
>    `cd ..`
>                  or
>    `cd <path-to-cloned-project-directory>`
>
> 7. docker-compose down
>    `docker-compose down`
>
> 8. build image
>    `docker-compose build`
>
> 9. docker-compose up
>    `docker-compose up`
>
> 10. Application is served at localhost:8080

## Running unit test suite
> **Running unit tests after project installation**
>
> 1. Navigate to project directory
>    `cd <path-to-project-dirctory>`
>
> 2. Navigate to frontend folder (search-for-fruits)
>    `cd search-for-fruits`
>
> 3. Run jest test suite single run
>    `yarn test`
>
