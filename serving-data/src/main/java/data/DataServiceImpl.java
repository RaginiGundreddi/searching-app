package data;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.beans.factory.annotation.Autowired;

@Service
@Validated
public class DataServiceImpl implements DataService {

  @Override
    public String[] getFilteredData(String searchText) {
      String[] fruits = {"Apple",
"Apricot",
"Avocado",
"Banana",
"Bilberry",
"Blackberry",
"Blackcurrant",
"Blueberry",
"Boysenberry",
"Currant",
"Cherry",
"Cherimoya",
"Cloudberry",
"Coconut",
"Cranberry",
"Cucumber",
"Custard apple",
"Damson",
"Date",
"Dragonfruit",
"Durian",
"Elderberry",
"Feijoa",
"Fig",
"Goji berry",
"Gooseberry",
"Grape",
"Raisin",
"Grapefruit",
"Guava",
"Honeyberry",
"Huckleberry",
"Jabuticaba",
"Jackfruit",
"Jambul",
"Jujube",
"Juniper berry",
"Kiwifruit",
"Kumquat",
"Lemon",
"Lime",
"Loquat",
"Longan",
"Lychee",
"Mango",
"Marionberry",
"Melon",
"Cantaloupe",
"Honeydew",
"Watermelon",
"Miracle fruit",
"Mulberry",
"Nectarine",
"Nance",
"Olive",
"Orange",
"Blood orange",
"Clementine",
"Mandarine",
"Tangerine",
"Papaya",
"Passionfruit",
"Peach",
"Pear",
"Persimmon",
"Physalis",
"Plantain",
"Plum",
"Prune (dried plum)",
"Pineapple",
"Plumcot (or Pluot)",
"Pomegranate",
"Pomelo",
"Purple mangosteen",
"Quince",
"Raspberry",
"Salmonberry",
"Rambutan",
"Redcurrant",
"Salal berry",
"Salak",
"Satsuma",
"Soursop",
"Star fruit",
"Solanum quitoense",
"Strawberry",
"Tamarillo",
"Tamarind",
"Ugli fruit",
"Yuzu"};
List<String> filteredList = new ArrayList<String>();
// String[] filteredFruits = {};
for (int i = 0; i < fruits.length; i++) {

    if (fruits[i].toLowerCase().contains(searchText.toLowerCase())) {
      filteredList.add(fruits[i]);
        // System.out.println(fruits[i]);
    }
}
String[] filteredFruits = new String[filteredList.size()];
filteredFruits = filteredList.toArray(filteredFruits);
Arrays.sort(filteredFruits);
// System.out.println(filteredFruits);
      if(searchText.equals("")) {
        return new String[] {};
       } else {
        return filteredFruits;
        // return new String[] {"ho", "oh"};
       }
    }



}
