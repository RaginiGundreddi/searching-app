package data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class dataController {
  @Autowired
  private DataService service;

    // private static final String template = "Hello, %s!";
    // private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/data")
    public String[] data(@RequestParam String searchCriteria) {
      String[] filteredData = service.getFilteredData(searchCriteria);
      // System.out.println(searchCriteria);
        // return new String[] {"hi", "oh"};
        return filteredData;
    }
}
