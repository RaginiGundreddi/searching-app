/*
    ./client/index.js
*/
import React from 'react';
import ReactDOM from 'react-dom';
import ProcessData from './component/ProcessData.react.js';

ReactDOM.render(<ProcessData />, document.getElementById('root'));
