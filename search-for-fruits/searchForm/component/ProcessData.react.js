import React from 'react';
import axios from 'axios';

function DisplayList(props) {
  return (
  <ul>
    {props.data.map(disp =>
      <li>{disp}</li>
    )}
  </ul>
  );
}

export default class ProcessData extends React.Component {

constructor(props) {
     super(props);
     this.state = {
       data: [],
       searchfor: '',
     };
     this.handleChange = this.handleChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
   }

handleSubmit(event) {
event.preventDefault();
var temp = this.state.searchfor;
var data = [];
var that = this;
axios.get('/data', {
    params: {
      searchCriteria: temp
    }
  })
  .then( (response) => {
  for(var i=0; i < response.data.length; i=i+1){
  data.push(response.data[i]);
  }
  that.setState({data: data});
  })
}

handleChange(event) {
    this.setState({searchfor: event.target.value});
  }

  render() {
    return (
     <div>
        <h1>Search For A Fruit - Type a text in search box and press enter to get fruit names that have that text in them (Case Insensitive)</h1>
        <form onSubmit={this.handleSubmit}>
          <input type="text" value={this.state.searchfor} onChange={this.handleChange}/>
        </form>
        <DisplayList data={this.state.data}/>
      </div>
      );
  }
}
