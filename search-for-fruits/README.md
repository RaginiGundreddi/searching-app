# Search-for-fruits


## Pre-installation Software Requirements
> 1. [yarn] (https://yarnpkg.com/en/)
>
> 2. [git v2.10.x+](https://git-scm.com/)
>
> 3. [nginx](https://nginx.org/en/download.html)
>    Alternatively you can install nginx using brew if using mac.
>    `brew install nginx`
>




## nginx configuration for local Development
> nginx installation creates an nginx.conf file within the directory /usr/local/etc/nginx.
> The following configuration is required in order to run the Frontend via nginx:
>


```
 #user  nobody;
 worker_processes  1;
 events {
    worker_connections  1024;
 }
 http {
  include       mime.types;
  default_type  application/octet-stream;
  server {
    listen 3000;
    server_name localhost;
    index index.html;
    root   /<PATH TO project>/dist;
    location / {
      try_files $uri$args $uri$args/ $uri $uri/ /index.html =404;
    }
    location /data {
           proxy_pass http://localhost:8080/data;
    }
  }
  include servers/*;
}
```


## Project Installation
> **Project Installation occurs from terminal or command prompt**
>
> 1. Clone project from gitlab
>
> 2. Navigate to new project directory
>    `cd <path-to-cloned-project-directory>`
>
> 3. Package installation (cloning git will also include the node_modules in the project structure. No need to install explicitly)
>    `yarn install`
>
> 4. Build dist watcher
>    `yarn build:local`
>
> 5. Starting nginx (password required)
>    `sudo nginx`
>
> 6. Application is served at localhost:3000

## Running unit test suite
> **Running unit tests after project installation**
>
> 1. Navigate to project directory
>    `cd <path-to-project-dirctory>`
>
> 2. Run karma-jasmine test suite single run
>    `yarn test`
>
